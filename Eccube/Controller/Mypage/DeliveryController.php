<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eccube\Controller\Mypage;

use Eccube\Controller\Admin\AbstractCsvImportController;
use Eccube\Entity\Master\Pref;
use Eccube\Controller\AbstractController;
use Eccube\Entity\BaseInfo;
use Eccube\Entity\CustomerAddress;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Eccube\Form\Type\Front\CustomerAddressType;
use Eccube\Repository\BaseInfoRepository;
use Eccube\Repository\CustomerAddressRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class DeliveryController extends AbstractCsvImportController
{
    /**
     * @var BaseInfo
     */
    protected $BaseInfo;

    /**
     * @var CustomerAddressRepository
     */
    protected $customerAddressRepository;

    public function __construct(BaseInfoRepository $baseInfoRepository, CustomerAddressRepository $customerAddressRepository)
    {
        $this->BaseInfo = $baseInfoRepository->get();
        $this->customerAddressRepository = $customerAddressRepository;
    }

    /**
     * お届け先一覧画面.
     *
     * @Route("/mypage/delivery", name="mypage_delivery")
     * @Template("Mypage/delivery.twig")
     */
    public function index(Request $request)
    {
        $Customer = $this->getUser();

        return [
            'Customer' => $Customer,
        ];
	}

	private function remove_utf8_bom($text)
	{
		$bom = pack('H*','EFBBBF');
		$text = preg_replace("/^$bom/", '', $text);
		return $text;
	}

	/**
     * お届け先一覧画面.
     *
     * @Route("/mypage/delivery/csv", name="mypage_delivery_upload_csv")     
     */
	public function upload_csv(Request $request) 
	{
		$submittedToken = $request->request->get('token');
		if (!$this->isCsrfTokenValid('csv-token', $submittedToken)) {
			throw new \Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException();			
		}
		$Customer = $this->getUser();
		if ($request->getMethod() === 'POST') {

			$formFile = $request->files->get('csv');

			if (!empty($formFile)) {

				$this->csvFileName = 'upload_'.\Eccube\Util\StringUtil::random().'.'.$formFile->getClientOriginalExtension();
				$formFile->move($this->eccubeConfig['eccube_csv_temp_realdir'], $this->csvFileName);
		
				// $file = file_get_contents($this->eccubeConfig['eccube_csv_temp_realdir'].'/'.$this->csvFileName);

				if (($file = fopen($this->eccubeConfig['eccube_csv_temp_realdir'].'/'.$this->csvFileName, "r")) !== FALSE) 
				{
					$Address = [];
					$isDataValid = true;
					$line = 1;
					while (($row = fgetcsv($file,1000,",")) !== FALSE)
					{
						//--skip row header with # sign
						$row[0] = $this->remove_utf8_bom($row[0]);
						$row[1] = $this->remove_utf8_bom($row[1]);						
						if (isset($row[0]) && substr($row[0], 0, 1) == "#") {
							$line++;
							continue;
						}

						// --validation of each row
						if (
							!isset($row[0]) || !isset($row[1]) || 
							!isset($row[2]) || !isset($row[3]) || 
							!isset($row[4]) ||
							empty($row[0]) || empty($row[1]) || 
							empty($row[2]) || empty($row[3]) || 
							empty($row[4])
						) 
						{
							$message = "CSVが不正でお届け先が追加できません";
							$isDataValid = false;
							break;
						};

						// --sanitize zip code
						$zipCode =  $row[0];
						$purifyZip = str_replace("-", "", $zipCode); //--remove hyphen
						if (strlen($purifyZip) < 7) {
							$message = sprintf("%s行目の郵便番号「%s」が無効です。", $line, $zipCode);
							$isDataValid = false;
							break;
						}

						// --prefecture existence in db
						$prefecture = null;
						try {							
							$name = $row[1];
							$query = $this->entityManager->createQueryBuilder()
										->select("p")
										->from("\\Eccube\\Entity\\Master\\Pref", "p")
										->where('p.name = :name')
										->setParameter('name', $name)
										->setMaxResults(1)
										->getQuery();
							$prefecture = $query->getSingleResult();
						}
						catch (\Doctrine\ORM\NoResultException $e) {							
						}
							
						if ($prefecture == null) {
							$message = sprintf("%s行目の都道府県「%s」が無効です。", $line, $name);
							$isDataValid = false;
							break;
						}

						$Address[] = [
							'zipcode' => $purifyZip,
							'prefecture' => $row[1],
							'city' => $row[2],
							'address' => $row[3],
							'phonenumber' => $row[4],
							'companyname' => $row[5],
							'prefecture_obj' => $prefecture,
						];
						$line++;
					} //-- end while
					fclose($file);

					if (!$isDataValid) 
					{
						$this->addFlash('csv_err', $message);
					}
					else 
					{
						// --Creating new customer addresses
						foreach	($Address as $line => $address) {													
							$prefecture = $address['prefecture_obj'];
							$city = isset($address['city']) ? $address['city'] : '';
							$address = isset($address['address']) ? $address['address'] : '';
							$CustomerAddress = new CustomerAddress();
							$CustomerAddress->setCustomer($Customer);
							$CustomerAddress->setPref($prefecture);
							$CustomerAddress->setName01($Customer->getName01());
							$CustomerAddress->setName02($Customer->getName02());
							$CustomerAddress->setKana01($Customer->getKana01());
							$CustomerAddress->setKana02($Customer->getKana02());

							$CustomerAddress->setPostalCode($Address[$line]['zipcode']);
							$CustomerAddress->setAddr01($city);
							$CustomerAddress->setAddr02($address);
							$CustomerAddress->setPhoneNumber($Address[$line]['phonenumber']);
							$CustomerAddress->setCompanyName($Address[$line]['companyname']);

							$this->entityManager->persist($CustomerAddress);
							$this->entityManager->flush();
						}
					}
					
					
				} //--fopen
				$this->removeUploadedFile();
			}
            
        }

		return $this->redirect($this->generateUrl('mypage_delivery'));
	}

	protected function getColumnConfig()
    {
        return [
            'zipcode' => [
                'name' => '#郵便番号 (必須)',
                'description' => '#郵便番号 (必須)',
                'required' => false,
			],
			'prefecture' => [
                'name' => '都道府県 (必須)',
                'description' => '',
                'required' => false,
			],
			'city' => [
                'name' => '市区町村 (必須)',
                'description' => '',
                'required' => false,
			],
			'address' => [
                'name' => '番地・ビル名 (必須)',
                'description' => '',
                'required' => false,
            ],            
            'phonenumber' => [
                'name' => '電話番号 (必須)',
                'description' => 'date',
                'required' => false,
            ],
            'companyname' => [
                'name' => '会社名 (※)',
                'description' => 'date',
                'required' => false,
            ],
            'name' => [
                'name' => '宛名',
                'description' => 'date',
                'required' => false,
            ],
        ];
	}
	

    /**
     * お届け先編集画面.
     *
     * @Route("/mypage/delivery/new", name="mypage_delivery_new")
     * @Route("/mypage/delivery/{id}/edit", name="mypage_delivery_edit", requirements={"id" = "\d+"})
     * @Template("Mypage/delivery_edit.twig")
     */
    public function edit(Request $request, $id = null)
    {
        $Customer = $this->getUser();

        // 配送先住所最大値判定
        // $idが存在する際は、追加処理ではなく、編集の処理ため本ロジックスキップ
        if (is_null($id)) {
            $addressCurrNum = count($Customer->getCustomerAddresses());
            $addressMax = $this->eccubeConfig['eccube_deliv_addr_max'];
            if ($addressCurrNum >= $addressMax) {
                throw new NotFoundHttpException();
            }
            $CustomerAddress = new CustomerAddress();
            $CustomerAddress->setCustomer($Customer);
        } else {
            $CustomerAddress = $this->customerAddressRepository->findOneBy(
                [
                    'id' => $id,
                    'Customer' => $Customer,
                ]
            );
            if (!$CustomerAddress) {
                throw new NotFoundHttpException();
            }
        }

        $parentPage = $request->get('parent_page', null);

        // 正しい遷移かをチェック
        $allowedParents = [
            $this->generateUrl('mypage_delivery'),
            $this->generateUrl('shopping_redirect_to'),
        ];

        // 遷移が正しくない場合、デフォルトであるマイページの配送先追加の画面を設定する
        if (!in_array($parentPage, $allowedParents)) {
            // @deprecated 使用されていないコード
            $parentPage = $this->generateUrl('mypage_delivery');
        }

        $builder = $this->formFactory
            ->createBuilder(CustomerAddressType::class, $CustomerAddress);

        $event = new EventArgs(
            [
                'builder' => $builder,
                'Customer' => $Customer,
                'CustomerAddress' => $CustomerAddress,
            ],
            $request
        );
        $this->eventDispatcher->dispatch(EccubeEvents::FRONT_MYPAGE_DELIVERY_EDIT_INITIALIZE, $event);

        $form = $builder->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            log_info('お届け先登録開始', [$id]);

            $this->entityManager->persist($CustomerAddress);
            $this->entityManager->flush();

            log_info('お届け先登録完了', [$id]);

            $event = new EventArgs(
                [
                    'form' => $form,
                    'Customer' => $Customer,
                    'CustomerAddress' => $CustomerAddress,
                ],
                $request
            );
            $this->eventDispatcher->dispatch(EccubeEvents::FRONT_MYPAGE_DELIVERY_EDIT_COMPLETE, $event);

            $this->addSuccess('mypage.delivery.add.complete');

            return $this->redirect($this->generateUrl('mypage_delivery'));
        }

        return [
            'form' => $form->createView(),
            'parentPage' => $parentPage,
            'BaseInfo' => $this->BaseInfo,
        ];
    }

    /**
     * お届け先を削除する.
     *
     * @Route("/mypage/delivery/{id}/delete", name="mypage_delivery_delete", methods={"DELETE"})
     */
    public function delete(Request $request, CustomerAddress $CustomerAddress)
    {
        $this->isTokenValid();

        log_info('お届け先削除開始', [$CustomerAddress->getId()]);

        $Customer = $this->getUser();

        if ($Customer->getId() != $CustomerAddress->getCustomer()->getId()) {
            throw new BadRequestHttpException();
        }

        $this->customerAddressRepository->delete($CustomerAddress);

        $event = new EventArgs(
            [
                'Customer' => $Customer,
                'CustomerAddress' => $CustomerAddress,
            ], $request
        );
        $this->eventDispatcher->dispatch(EccubeEvents::FRONT_MYPAGE_DELIVERY_DELETE_COMPLETE, $event);

        $this->addSuccess('mypage.address.delete.complete');

        log_info('お届け先削除完了', [$CustomerAddress->getId()]);

        return $this->redirect($this->generateUrl('mypage_delivery'));
    }
}
