<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eccube\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;

class TopController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @Template("index.twig")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $RAW_QUERY = 'SELECT * FROM dtb_category WHERE parent_category_id IS NULL';
        $statement = $em->getConnection()->prepare($RAW_QUERY);
        $statement->execute();
        $worktypes = $statement->fetchAll();
        
        return [
            'worktypes' => $worktypes
          
        ];
    }
}
