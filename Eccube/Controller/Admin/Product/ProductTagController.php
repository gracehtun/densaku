<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eccube\Controller\Admin\Product;

use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Eccube\Common\Constant;
use Eccube\Controller\AbstractController;
use Eccube\Entity\BaseInfo;
use Eccube\Entity\Product;
use Eccube\Entity\ProductCategory;
use Eccube\Entity\ProductClass;
use Eccube\Entity\ProductImage;
use Eccube\Entity\ProductTag;
use Eccube\Entity\Supplier;
use Eccube\Event\EccubeEvents;
use Eccube\Event\EventArgs;
use Eccube\Form\Type\Admin\ProductType;
use Eccube\Form\Type\Admin\SearchProductTagType;
use Eccube\Repository\BaseInfoRepository;
use Eccube\Repository\Master\PageMaxRepository;
use Eccube\Repository\ProductClassRepository;
use Eccube\Repository\ProductImageRepository;
use Eccube\Repository\ProductRepository;
use Eccube\Repository\TagRepository;
use Eccube\Repository\ProductTagRepository;
use Eccube\Util\CacheUtil;
use Eccube\Util\FormUtil;
use Knp\Component\Pager\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

class ProductTagController extends AbstractController
{

    /**
     * @var ProductClassRepository
     */
    protected $productClassRepository;

    /**
     * @var ProductImageRepository
     */
    protected $productImageRepository;

    /**
     * @var ProductRepository
     */
    protected $productRepository;

    /**
     * @var BaseInfo
     */
    protected $BaseInfo;

    /**
     * @var PageMaxRepository
     */
    protected $pageMaxRepository;

    /**
     * @var TagRepository
     */
    protected $tagRepository;

     /**
     * @var ProductTagRepository
     */
    protected $productTagRepository;

    /**
     * ProductController constructor.
     *
     * @param ProductClassRepository $productClassRepository
     * @param ProductImageRepository $productImageRepository
     * @param ProductRepository $productRepository
     * @param BaseInfoRepository $baseInfoRepository
     * @param PageMaxRepository $pageMaxRepository
     * @param TagRepository $tagRepository
     */
    public function __construct(
        ProductClassRepository $productClassRepository,
        ProductImageRepository $productImageRepository,
        ProductRepository $productRepository,
        BaseInfoRepository $baseInfoRepository,
        PageMaxRepository $pageMaxRepository,
        TagRepository $tagRepository
    ) {
        $this->productClassRepository = $productClassRepository;
        $this->productImageRepository = $productImageRepository;
        $this->productRepository = $productRepository;
        $this->BaseInfo = $baseInfoRepository->get();
        $this->pageMaxRepository = $pageMaxRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * @Route("/%eccube_admin_route%/producttag", name="admin_producttag")
     * @Route("/%eccube_admin_route%/producttag/page/{page_no}", requirements={"page_no" = "\d+"}, name="admin_producttag_page")
     * @Template("@admin/Tag/index.twig")
     */
    public function index(Request $request, $page_no = null, Paginator $paginator)
    {
        $builder = $this->formFactory
            ->createBuilder(SearchProductTagType::class);

        $event = new EventArgs(
            [
                'builder' => $builder,
            ],
            $request
        );
        $this->eventDispatcher->dispatch(EccubeEvents::ADMIN_PRODUCT_INDEX_INITIALIZE, $event);

        $searchForm = $builder->getForm();

        $page_count = $this->session->get('eccube.admin.order.search.page_count_tag',
            $this->eccubeConfig->get('eccube_default_page_count'));

        $page_count_param = (int) $request->get('page_count');
        $pageMaxis = $this->pageMaxRepository->findAll();

        if ($page_count_param) {
            foreach ($pageMaxis as $pageMax) {
                if ($page_count_param == $pageMax->getName()) {
                    $page_count = $pageMax->getName();
                    $this->session->set('eccube.admin.order.search.page_count_tag', $page_count);
                    break;
                }
            }
        }

        if ('POST' === $request->getMethod()) {
            $searchForm->handleRequest($request);

            if ($searchForm->isValid()) {

                $page_no = 1;
                $searchData = $searchForm->getData();

                $this->session->set('eccube.admin.product.search_tag', FormUtil::getViewData($searchForm));
                $this->session->set('eccube.admin.product.search_tag.page_no', $page_no);
            } else {

                return [
                    'searchForm' => $searchForm->createView(),
                    'pagination' => [],
                    'pageMaxis' => $pageMaxis,
                    'page_no' => $page_no,
                    'page_count' => $page_count,
                    'has_errors' => true,
                ];
            }
        } else {
            if (null !== $page_no || $request->get('resume')) {
                /*
                 * ページ送りの場合または、他画面から戻ってきた場合は, セッションから検索条件を復旧する.
                 */
                if ($page_no) {
                    // ページ送りで遷移した場合.
                    $this->session->set('eccube.admin.product.search_tag.page_no', (int) $page_no);
                } else {
                    // 他画面から遷移した場合.
                    $page_no = $this->session->get('eccube.admin.product.search_tag.page_no', 1);
                }
                $viewData = $this->session->get('eccube.admin.product.search_tag', []);

                $searchData = FormUtil::submitAndGetData($searchForm, $viewData);
            } else {
                /**
                 * 初期表示の場合.
                 */
                $page_no = 1;
                // submit default value
                $viewData = FormUtil::getViewData($searchForm);
                $searchData = FormUtil::submitAndGetData($searchForm, $viewData);

                // セッション中の検索条件, ページ番号を初期化.
                $this->session->set('eccube.admin.product.search_tag', $viewData);
                $this->session->set('eccube.admin.product.search_tag.page_no', $page_no);
            }
        }
        $conn = $this->entityManager->getConnection();
        if(!empty($searchData['tag'])){
                foreach ($searchData['tag'] as $key => $value) {
                    $stmt = $conn->query("SELECT id FROM dtb_tag WHERE name = '".$value."'");
                    $row[] = $stmt->fetch();
                }
            unset($searchData['tag']);
            $searchData['tag'] = array();

            foreach ($row as $key => $value) {
                $searchData['tag'][] = $value['id'];
            }
        }
        foreach ($searchData['tag'] as $key => $value) {
            $stmt = $conn->query("SELECT product_id,tag_id FROM dtb_product_tag WHERE tag_id = '".$value."'");
            $aryResult = $stmt->fetchAll();
            if($aryResult != false)
            $productId[] = $aryResult;
        }
        foreach ($productId as $key => $value) {
            foreach ($value as $k => $v) {
                $searchData['product_id'][$v['tag_id']][] = $v['product_id'];
            }
           
        }
       
        if(count($searchData['product_id']) != 1){
            $searchData['product_id'] = array_intersect(...$searchData['product_id']);}
        else{
            foreach ($searchData['product_id'] as $key => $value) {
                $searchData['product_id'] = $value;
            }
        }
      
        $qb = $this->productRepository->getQueryBuilderBySearchDataTagForAdmin($searchData);
        
        $event = new EventArgs(
            [
                'qb' => $qb,
                'searchData' => $searchData,
            ],
            $request
        );

        $this->eventDispatcher->dispatch(EccubeEvents::ADMIN_PRODUCT_INDEX_SEARCH, $event);


        $pagination = $paginator->paginate(
            $qb,
            $page_no,
            $page_count
        );

        return [
            'searchForm' => $searchForm->createView(),
            'pagination' => $pagination,
            'pageMaxis' => $pageMaxis,
            'page_no' => $page_no,
            'page_count' => $page_count,
            'has_errors' => false,
        ];
    }

    /**
     * tag change function
     *
     * @Route("/%eccube_admin_route%/product/tag/change/{id}", requirements={"id" = "\d+"}, name="admin_product_product_tag", methods={"POST"})
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function tagChange(Request $request, CacheUtil $cacheUtil)
    {
        $this->isTokenValid();
        
        $updateValue = $request->get('product_tag');
        $aryTag = array();
        $row = array();

        if(isset($updateValue)){
        foreach ($updateValue as $key => $value) {
            foreach ($value as $value) {
                $aryTag[$key][] = $value;
            }          
            }
        }

        if(isset($aryTag)) {

            foreach ($aryTag as $key => $value) {
                foreach ($value as $value) {
                   $conn = $this->entityManager->getConnection();
                        $stmt = $conn->query("SELECT id FROM dtb_tag WHERE name = '".$value."'");
                        $row[$key][] = $stmt->fetch();
                }
            }
       
            foreach ($row as $key => $value) {
                $sql = "delete from dtb_product_tag where dtb_product_tag.product_id = :fieldoneid";
                $params = array('fieldoneid'=>$key);

                $em = $this->getDoctrine()->getManager();
                $stmt = $em->getConnection()->prepare($sql);
                $stmt->execute($params);
            }
            foreach ($row as $key => $value) {

                $value = array_filter($value);

                foreach ($value as $value) {

                $sth = $conn->prepare("INSERT INTO dtb_product_tag (product_id, tag_id, creator_id, create_date, discriminator_type) VALUES (:login_id, :password , '1', current_timestamp, 'producttag');");
                $sth->execute([
                ':login_id' => $key,
                ':password' => $value['id'],
                ]);
                }
            }
        }
        return $this->redirectToRoute('admin_producttag', ['resume' => Constant::ENABLED]);
    }
}
