<?php
namespace Eccube\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Eccube\Entity\SupplierStore;
use Eccube\Entity\Supplier;
use Eccube\Entity\Product;
use Eccube\Entity\TaxRule;
use Eccube\Entity\Category;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
class SupplierStoreController extends AbstractController
{
    //waizin
    /**
     * 商品詳細画面.
     *
     * @Route("/getsupplierstore", name="detail_info", methods={"GET"}, requirements={"id" = "\d+"})
     * @Template("SupplierStore/detail_info.twig")
     *
     */
    public function getsupplierstore(Request $request)
    {  
        $id =   $request->query->get('id'); 
        $supplierWorkItem = ""; 
        $workitem = array();
        $pagi_product_list = array(); 
        $supplierstore = $this->getDoctrine()
        ->getRepository(SupplierStore::class)
        ->findBy(array("supplier_id"=>$id));
        $supplier = $this->getDoctrine()
        ->getRepository(Supplier::class)
        ->findBy(array("supplier_id"=>$id));
        if(count($supplier) >0){
            $work_item = explode(",",$supplier[0]['work_item']);
            if(count($work_item)>0){
                for($x = 0; $x < count($work_item); $x++){
                    $workItemName =   $this->getDoctrine()->getRepository(Category::class)->findBy(array("id"=>$work_item[$x]));
                    array_push($workitem,$workItemName[0]['name']);
                }
            }
        }
        $supplierWorkItem = implode(",", $workitem);
        $supplier_product = $this->getDoctrine()
        ->getRepository(Product::class)
        ->findBy(array('supplier_id' => $id));
        if(count($supplier_product) > 0){
            $product_id = $supplier_product[0]['id'];
            $tax = $this->getDoctrine()->getRepository(TaxRule::class)->findAll();
            $taxRate = ($tax[0]['tax_rate'])/100;
            $em = $this->getDoctrine()->getManager();
            $RAW_QUERY = 'SELECT p.*, pimg.file_name, pclass.price02 FROM dtb_product p 
                            LEFT JOIN dtb_product_image pimg ON pimg.product_id = p.id 
                            LEFT JOIN dtb_product_class pclass ON pclass.product_id = p.id
                            WHERE p.product_status_id = 1 AND p.supplier_id ="'.$id.'" AND p.id >='.$product_id.' 
                            GROUP BY p.id ,pimg.file_name, pclass.price02;';
            $statement = $em->getConnection()->prepare($RAW_QUERY);
            $statement->execute();
            $supplier_product_list = $statement->fetchAll();
            $supplier_product_list =$this->group_by("id", $supplier_product_list);
            $paginator = $this->get('knp_paginator');
            $pagi_product_list = $paginator->paginate(
            $supplier_product_list, $request->query->getInt('page', 1), 5);
        }
         return [
            'title' =>"",
            'supplierstore' =>$supplierstore[0],
            'supplier' =>$supplier[0],
            'workitem' => $supplierWorkItem,
            'pagi_product_list' =>$pagi_product_list,
            'taxRate' =>$taxRate,
            ];
    }
    //zayar phone naing
    /**
     * 
     * @Route("/supplierlist", name="supplier_list", methods={"GET"}, requirements={"pref" = "\d+"})
     * @Template("SupplierStore/supplier_list.twig")
     *
     */
    public function supplierlist(Request $request)
    {  
        $prefecture = $request->query->get('pref');
        $supplierlist = $this->getDoctrine()->getRepository(Supplier::class)->findBy(array('prefecture' => $prefecture, 'active' => 1));
         return [
            'prefecture' => $prefecture,
            'supplierlists' => $supplierlist
        ];
    }
    function group_by($key ,$array) {
        $temp_array = array();
        $i = 0;
        $key_array = array();
        foreach($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $temp_array[$i] = $val;
            }
            $i++;
        }
        return $temp_array;
    }
}