<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eccube\Form\Type\Admin;

use Eccube\Entity\Category;
use Eccube\Entity\Master\ProductStatus;
use Eccube\Entity\ProductStock;
use Eccube\Form\Type\Master\CategoryType as MasterCategoryType;
use Eccube\Form\Type\Master\ProductStatusType;
use Eccube\Repository\CategoryRepository;
use Eccube\Repository\Master\ProductStatusRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchProductTagType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', TextType::class, [
                'label' => 'admin.product.multi_search_label',
                'required' => false,
            ])
            ->add('supplier_id', TextType::class, [
                'label' => 'admin.supplier.multi_search_label',
                'required' => false,
            ])
            ->add('tag', ChoiceType::class, [
                'label' => 'admin.product.stock',
                'choices' => [
                    '春' => '春',
                    '夏' => '夏',
                    '秋' => '秋',
                    '冬' => '冬',
                    'お中元　お歳暮' => 'お中元　お歳暮',
                    'ピックアップ' => 'ピックアップ',
                ],
                'expanded' => true,
                'multiple' => true,
                //'data' => ['春','夏','秋','冬','お中元　お歳暮','ピックアップ'],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'admin_search_producttag';
    }
}
