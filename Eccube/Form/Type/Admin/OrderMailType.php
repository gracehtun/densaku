<?php

/*
 * This file is part of EC-CUBE
 *
 * Copyright(c) LOCKON CO.,LTD. All Rights Reserved.
 *
 * http://www.lockon.co.jp/
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Eccube\Form\Type\Admin;

use Eccube\Common\EccubeConfig;
use Eccube\Form\Type\Master\MailTemplateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Eccube\Form\Validator\TwigLint;
use Doctrine\ORM\EntityRepository;

use Eccube\Entity\Supplier;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Eccube\Repository\SupplierRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderMailType extends AbstractType
{
    /**
     * @var EccubeConfig
     */
	protected $eccubeConfig;
	
	/**
     * @var SupplierRepository
     */
    protected $supplierRepository;

    /**
     * MailType constructor.
     *
     * @param EccubeConfig $eccubeConfig
     */
    public function __construct(
		EccubeConfig $eccubeConfig,
		SupplierRepository $supplierRepository
    ) {
		$this->eccubeConfig = $eccubeConfig;
		$this->supplierRepository = $supplierRepository;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('template', MailTemplateType::class, [
                'required' => false,
                'mapped' => false,
                'query_builder' => function (EntityRepository $er) {
                    // return $er->createQueryBuilder('mt')
                    //     ->andWhere('mt.id = :id')
                    //     ->setParameter('id', $this->eccubeConfig['eccube_order_mail_template_id'])
					//     ->orderBy('mt.id', 'ASC');
					$qb = $er->createQueryBuilder('mt');
					$qb->andWhere($qb->expr()->in('mt.id', ':ids'))
						->setParameter('ids', [$this->eccubeConfig['eccube_order_mail_template_id'], 9, 10, 11, 12])
						->orderBy('mt.id', 'ASC');
					return $qb;

                },
            ])
            ->add('mail_subject', TextType::class, [
                'required' => true,
                'constraints' => [
                    new Assert\NotBlank(),
                ],
            ])
            ->add('tpl_data', TextareaType::class, [
                'label' => false,
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new TwigLint(),
                ],
			])
			->add('Suppliers', ChoiceType::class, [
				'choice_label' => function (Supplier $supplier =  null) {
					return $supplier ? sprintf("%s (メールアドレス : %s)", $supplier->getTradeName(), $supplier->getEmail()) : null;
				},
                'multiple' => true,
                'mapped' => false,
                'expanded' => true,
                'choices' => $this->supplierRepository->getOrderSuppliers($options['order_id']),
                'choice_value' => function (Supplier $Supplier = null) {
                    return $Supplier ? $Supplier->getId() : null;
                },
			])
        ;
    }

	public function configureOptions(OptionsResolver $resolver) 
	{
		$resolver->setDefaults([
			'order_id' => 0,
		]);
	}

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'admin_order_mail';
    }
}
