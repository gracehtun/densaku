<?php
namespace Eccube\Repository;
use Doctrine\Common\Collections\ArrayCollection;
use Eccube\Common\EccubeConfig;
use Eccube\Entity\SupplierStore;
use Symfony\Bridge\Doctrine\RegistryInterface;
class SupplierStoreRepository extends AbstractRepository
{
     /**
     * @var EccubeConfig
     */
    protected $eccubeConfig;
      /**
     * SupplierStoreRepository constructor.
     *
     * @param RegistryInterface $registry
     * @param EccubeConfig $eccubeConfig
     */
    public function __construct(
        RegistryInterface $registry,
        EccubeConfig $eccubeConfig
    ) {
        parent::__construct($registry, SupplierStore::class);
        $this->eccubeConfig = $eccubeConfig;
    }
}