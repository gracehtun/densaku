<?php
namespace Eccube\Repository;
use Doctrine\Common\Collections\ArrayCollection;
use Eccube\Common\EccubeConfig;
use Eccube\Entity\Supplier;
use Symfony\Bridge\Doctrine\RegistryInterface;
class SupplierRepository extends AbstractRepository
{
     /**
     * @var EccubeConfig
     */
    protected $eccubeConfig;
      /**
     * SupplierRepository constructor.
     *
     * @param RegistryInterface $registry
     * @param EccubeConfig $eccubeConfig
     */
    public function __construct(
        RegistryInterface $registry,
        EccubeConfig $eccubeConfig
    ) {
        parent::__construct($registry, Supplier::class);
        $this->eccubeConfig = $eccubeConfig;
    }
    public function getSupplier($id) 
    {
        return $this
            ->createQueryBuilder('s')
            ->where('s.id=:id')
            ->setParameter('id', $id)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
    public function getOrderSuppliers($orderId) 
    {
        $query = $this->createQueryBuilder('s')
                    ->innerJoin('s.Product', 'p')
                    ->innerJoin('p.OrderItems', 'oi')
                    ->innerJoin('oi.Order', 'o')
                    ->where('s.active = 1')
                    ->andWhere('o.id = :orderId')
                    ->setParameter('orderId', $orderId)
                    ->addOrderBy('oi.id')
                    ->getQuery();

        $Suppliers = $query->getResult();
        return $Suppliers;
    }
}