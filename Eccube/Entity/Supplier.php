<?php
namespace Eccube\Entity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
/**
 * Supplier
 *
 * @ORM\Table(name="dtb_supplier")
 * @ORM\Entity(repositoryClass="Eccube\Repository\SupplierRepository")
 */
class Supplier extends \Eccube\Entity\AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
     private $id;
    /**
     * @ORM\Column(type="string", length=11)
     */
    private $supplier_id;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $trade_name;
     /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;
    /**
     * @ORM\Column(type="integer", length=1, nullable=true)
     */
    private $charges_include;
    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $charges_type;
    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $charges_amount;
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $establishment_year;
    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private $zip;
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $prefecture;
    /**
     * @ORM\Column(type="string", length=300, nullable=true)
     */
    private $city;
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $address;
    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $phone_number;
    /**
     * @ORM\Column(type="integer", length=15, nullable=true)
     */
    private $fax;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $url;
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $employees;
    /**
     * @ORM\Column(type="string", length=200)
     */
    private $work_type;
    /**
     * @ORM\Column(type="string", length=800)
     */
    private $work_item;
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $bank;
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $branch;
    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $account_kind;
    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $account_number;
    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $account_name;
    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $active;
    /**
     * @ORM\Column(type="datetimetz", length=1)
     */
    private $create_date;
    /**
     * @ORM\Column(type="datetimetz", length=1)
     */
    private $update_date;
    /**
     * @ORM\Column(type="datetimetz", length=1, nullable=true)
     */
    private $delete_date;
    /**
     * @ORM\Column(type="datetimetz", length=1, nullable=true)
     */
    private $last_date;
     /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\OneToMany(targetEntity="Eccube\Entity\Product", mappedBy="Supplier")
     * 
     */
    private $Product;
    /**
     * Constructor
     */
    public function __construct()
    {
        //$this->Product = new \Doctrine\Common\Collections\ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }
    public function getSupplierId(): ?string
    {
        return $this->supplier_id;
    }
    public function setSupplierId(string $supplier_id): self
    {
        $this->supplier_id = $supplier_id;
        return $this;
    }
    public function getTradeName(): ?string
    {
        return $this->trade_name;
    }
    public function setTradeName(string $trade_name): self
    {
        $this->trade_name = $trade_name;
        return $this;
    }
    public function getEmail(): ?string
    {
        return $this->email;
    }
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }
    public function getPassword(): ?string
    {
        return $this->password;
    }
    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }
    public function getChargesInclude(): ?int
    {
        return $this->charges_include;
    }
    public function setChargesInclude(int $charges_include): self
    {
        $this->charges_include = $charges_include;
        return $this;
    }
    public function getChargesType(): ?int
    {
        return $this->charges_type;
    }
    public function setChargesType(int $charges_type): self
    {
        $this->charges_type = $charges_type;
        return $this;
    }
    public function getChargesAmount(): ?int
    {
        return $this->charges_amount;
    }
    public function setChargesAmount(int $charges_amount): self
    {
        $this->charges_amount = $charges_amount;
        return $this;
    }
    public function getEstablishmentYear(): ?string
    {
        return $this->establishment_year;
    }
    public function setEstablishmentYear(?string $establishment_year): self
    {
        $this->establishment_year = $establishment_year;
        return $this;
    }
    public function getZip(): ?string
    {
        return $this->zip;
    }
    public function setZip(?string $zip): self
    {
        $this->zip = $zip;
        return $this;
    }
    public function getPrefecture(): ?string
    {
        return $this->prefecture;
    }
    public function setPrefecture(?string $prefecture): self
    {
        $this->prefecture = $prefecture;
        return $this;
    }
    public function getCity(): ?string
    {
        return $this->city;
    }
    public function setCity(?string $city): self
    {
        $this->city = $city;
        return $this;
    }
    public function getAddress(): ?string
    {
        return $this->address;
    }
    public function setAddress(?string $address): self
    {
        $this->address = $address;
        return $this;
    }
    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }
    public function setPhoneNumber(?string $phone_no): self
    {
        $this->phone_number = $phone_number;
        return $this;
    }
    public function getFax(): ?string
    {
        return $this->fax;
    }
    public function setFax(?string $fax): self
    {
        $this->fax = $fax;
        return $this;
    }
    public function getUrl(): ?string
    {
        return $this->url;
    }
    public function setUrl(?string $url): self
    {
        $this->url = $url;
        return $this;
    }
    public function getEmployees(): ?string
    {
        return $this->employees;
    }
    public function setEmployees(?string $employees): self
    {
        $this->employees = $employees;
        return $this;
    }
    public function getWorkType(): ?string
    {
        return $this->work_type;
    }
    public function setWorkType(string $work_type): self
    {
        $this->work_type = $work_type;
        return $this;
    }
    public function getWorkItem(): ?string
    {
        return $this->work_item;
    }
    public function setWorkItem(string $work_item): self
    {
        $this->work_item = $work_item;
        return $this;
    }
    public function getBank(): ?string
    {
        return $this->bank;
    }
    public function setBank(string $bank): self
    {
        $this->bank = $bank;
        return $this;
    }
    public function getBranch(): ?string
    {
        return $this->branch;
    }
    public function setBranch(string $branch): self
    {
        $this->branch = $branch;
        return $this;
    }
    public function getAccountKind(): ?string
    {
        return $this->account_kind;
    }
    public function setAccountKind(string $account_kind): self
    {
        $this->account_kind = $account_kind;
        return $this;
    }
    public function getAccountNumber(): ?string
    {
        return $this->account_number;
    }
    public function setAccountNumber(string $account_number): self
    {
        $this->account_number = $account_number;
        return $this;
    }
    public function getAccountName(): ?string
    {
        return $this->account_name;
    }
    public function setAccountName(string $account_name): self
    {
        $this->account_name = $account_name;
        return $this;
    }
    public function getActive(): ?int
    {
        return $this->active;
    }
    public function setActive(int $active): self
    {
        $this->active = $active;
        return $this;
    }
    public function getCreateDate(): ?string
    {
        return $this->create_date;
    }
    public function setCreateDate(string $create_date): self
    {
        $this->create_date = $create_date;
        return $this;
    }
    public function getUpdateDate(): ?string
    {
        return $this->update_date;
    }
    public function setUpdateDate(string $update_date): self
    {
        $this->update_date = $update_date;
        return $this;
    }
    public function getDeleteDate(): ?string
    {
        return $this->delete_date;
    }
    public function setDeleteDate(string $delete_date): self
    {
        $this->delete_date = $delete_date;
        return $this;
    }
    public function getLastDate(): ?string
    {
        return $this->last_date;
    }
    public function setLastDate(string $last_date): self
    {
        $this->last_date = $last_date;
        return $this;
    }
}