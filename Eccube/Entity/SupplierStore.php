<?php
namespace Eccube\Entity;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
    /**
     * SupplierStore
     *
     * @ORM\Table(name="dtb_supplier_store")
     * @ORM\Entity(repositoryClass="Eccube\Repository\SupplierStoreRepository")
     */
    class SupplierStore extends \Eccube\Entity\AbstractEntity
    {
        /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=11)
     */
    private $supplier_id;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title_1;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $logo_1;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content_1;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $title_2;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $logo_2;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $content_2;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_1;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_title_1;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_2;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_title_2;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_3;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_title_3;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_4;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_title_4;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_5;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_title_5;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_6;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_title_6;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_7;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_title_7;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_8;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $image_title_8;
    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $movie_url;
    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $movie_content;
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telephone_1;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $telephone_2;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $telephone_3;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $fax_1;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $fax_2;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $fax_3;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $workitem;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $siteurl;
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $email;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $hide_addr;
    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $hide_email;
    /**
     * @ORM\Column(type="datetime")
     */
    private $create_date;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $update_date;
    public function getId(): ?int
    {
        return $this->id;
    }
    public function getSupplierId(): ?string
    {
        return $this->supplier_id;
    }
    public function setSupplierId(string $supplier_id): self
    {
        $this->supplier_id = $supplier_id;
        return $this;
    }
    public function getTitle1(): ?string
    {
        return $this->title_1;
    }
    public function setTitle1(?string $title_1): self
    {
        $this->title_1 = $title_1;
        return $this;
    }
    public function getLogo1(): ?string
    {
        return $this->logo_1;
    }
    public function setLogo1(?string $logo_1): self
    {
        $this->logo_1 = $logo_1;
        return $this;
    }
    public function getContent1(): ?string
    {
        return $this->content_1;
    }
    public function setContent1(?string $content_1): self
    {
        $this->content_1 = $content_1;
        return $this;
    }
    public function getTitle2(): ?string
    {
        return $this->title_2;
    }
    public function setTitle2(?string $title_2): self
    {
        $this->title_2 = $title_2;
        return $this;
    }
    public function getLogo2(): ?string
    {
        return $this->logo_2;
    }
    public function setLogo2(?string $logo_2): self
    {
        $this->logo_2 = $logo_2;
        return $this;
    }
    public function getContent2(): ?string
    {
        return $this->content_2;
    }
    public function setContent2(?string $content_2): self
    {
        $this->content_2 = $content_2;
        return $this;
    }
    public function getImage1(): ?string
    {
        return $this->image_1;
    }
    public function setImage1(?string $image_1): self
    {
        $this->image_1 = $image_1;
        return $this;
    }
    public function getImageTitle1(): ?string
    {
        return $this->image_title_1;
    }
    public function setImageTitle1(?string $image_title_1): self
    {
        $this->image_title_1 = $image_title_1;
        return $this;
    }
    public function getImage2(): ?string
    {
        return $this->image_2;
    }
    public function setImage2(?string $image_2): self
    {
        $this->image_2 = $image_2;
        return $this;
    }
    public function getImageTitle2(): ?string
    {
        return $this->image_title_2;
    }
    public function setImageTitle2(?string $image_title_2): self
    {
        $this->image_title_2 = $image_title_2;
        return $this;
    }
    public function getImage3(): ?string
    {
        return $this->image_3;
    }
    public function setImage3(?string $image_3): self
    {
        $this->image_3 = $image_3;
        return $this;
    }
    public function getImageTitle3(): ?string
    {
        return $this->image_title_3;
    }
    public function setImageTitle3(?string $image_title_3): self
    {
        $this->image_title_3 = $image_title_3;
        return $this;
    }
    public function getImage4(): ?string
    {
        return $this->image_4;
    }
    public function setImage4(?string $image_4): self
    {
        $this->image_4 = $image_4;
        return $this;
    }
    public function getImageTitle4(): ?string
    {
        return $this->image_title_4;
    }
    public function setImageTitle4(?string $image_title_4): self
    {
        $this->image_title_4 = $image_title_4;
        return $this;
    }
    public function getImage5(): ?string
    {
        return $this->image_5;
    }
    public function setImage5(?string $image_5): self
    {
        $this->image_5 = $image_5;
        return $this;
    }
    public function getImageTitle5(): ?string
    {
        return $this->image_title_5;
    }
    public function setImageTitle5(?string $image_title_5): self
    {
        $this->image_title_5 = $image_title_5;
        return $this;
    }
    public function getImage6(): ?string
    {
        return $this->image_6;
    }
    public function setImage6(?string $image_6): self
    {
        $this->image_6 = $image_6;
        return $this;
    }
    public function getImageTitle6(): ?string
    {
        return $this->image_title_6;
    }
    public function setImageTitle6(?string $image_title_6): self
    {
        $this->image_title_6 = $image_title_6;
        return $this;
    }
    public function getImage7(): ?string
    {
        return $this->image_7;
    }
    public function setImage7(?string $image_7): self
    {
        $this->image_7 = $image_7;
        return $this;
    }
    public function getImageTitle7(): ?string
    {
        return $this->image_title_7;
    }
    public function setImageTitle7(?string $image_title_7): self
    {
        $this->image_title_7 = $image_title_7;
        return $this;
    }
    public function getImage8(): ?string
    {
        return $this->image_8;
    }
    public function setImage8(?string $image_8): self
    {
        $this->image_8 = $image_8;
        return $this;
    }
    public function getImageTitle8(): ?string
    {
        return $this->image_title_8;
    }
    public function setImageTitle8(?string $image_title_8): self
    {
        $this->image_title_8 = $image_title_8;
        return $this;
    }
    public function getMovieUrl(): ?string
    {
        return $this->movie_url;
    }
    public function setMovieUrl(?string $movie_url): self
    {
        $this->movie_url = $movie_url;
        return $this;
    }
    public function getMovieContent(): ?string
    {
        return $this->movie_content;
    }
    public function setMovieContent(?string $movie_content): self
    {
        $this->movie_content = $movie_content;
        return $this;
    }
    public function getTelephone1(): ?string
    {
        return $this->telephone_1;
    }
    public function setTelephone1(?string $telephone_1): self
    {
        $this->telephone_1 = $telephone_1;
        return $this;
    }
    public function getTelephone2(): ?string
    {
        return $this->telephone_2;
    }
    public function setTelephone2(?string $telephone_2): self
    {
        $this->telephone_2 = $telephone_2;
        return $this;
    }
    public function getTelephone3(): ?string
    {
        return $this->telephone_3;
    }
    public function setTelephone3(?string $telephone_3): self
    {
        $this->telephone_3 = $telephone_3;
        return $this;
    }
    public function getFax1(): ?string
    {
        return $this->fax_1;
    }
    public function setFax1(?string $fax_1): self
    {
        $this->fax_1 = $fax_1;
        return $this;
    }
    public function getFax2(): ?string
    {
        return $this->fax_2;
    }
    public function setFax2(?string $fax_2): self
    {
        $this->fax_2 = $fax_2;
        return $this;
    }
    public function getFax3(): ?string
    {
        return $this->fax_3;
    }
    public function setFax3(?string $fax_3): self
    {
        $this->fax_3 = $fax_3;
        return $this;
    }
    public function getWorkitem(): ?string
    {
        return $this->workitem;
    }
    public function setWorkitem(?string $workitem): self
    {
        $this->workitem = $workitem;
        return $this;
    }
    public function getSiteurl(): ?string
    {
        return $this->siteurl;
    }
    public function setSiteurl(?string $siteurl): self
    {
        $this->siteurl = $siteurl;
        return $this;
    }
    public function getEmail(): ?string
    {
        return $this->email;
    }
    public function setEmail(?string $email): self
    {
        $this->email = $email;
        return $this;
    }
    public function getHideAddr(): ?int
    {
        return $this->hide_addr;
    }
    public function setHideAddr(?int $hide_addr): self
    {
        $this->hide_addr = $hide_addr;
        return $this;
    }
    public function getHideEmail(): ?int
    {
        return $this->hide_email;
    }
    public function setHideEmail(?int $hide_email): self
    {
        $this->hide_email = $hide_email;
        return $this;
    }
    public function getCreateDate(): ?\DateTimeInterface
    {
        return $this->create_date;
    }
    public function setCreateDate(\DateTimeInterface $create_date): self
    {
        $this->create_date = $create_date;
        return $this;
    }
    public function getUpdateDate(): ?\DateTimeInterface
    {
        return $this->update_date;
    }
    public function setUpdateDate(?\DateTimeInterface $update_date): self
    {
        $this->update_date = $update_date;
        return $this;
    }
    }